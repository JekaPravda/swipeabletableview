//
//  main.m
//  EPASwipeableTableView
//
//  Created by Евгений Правда on 7/10/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EPAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EPAAppDelegate class]));
    }
}
