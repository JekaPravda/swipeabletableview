//
//  EPARootViewController.m
//  EPASwipeableTableView
//
//  Created by Евгений Правда on 7/10/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPARootViewController.h"
#import "EPATableView.h"
@interface EPARootViewController ()
@property (strong, nonatomic) EPATableView *tableView;
@end

@implementation EPARootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor greenColor];
    self.tableView = [EPATableView new];
    [self.view addSubview:self.tableView];
    [self.tableView hideTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonClicked:(id)sender {
    [self.tableView showTableView];
}

@end
