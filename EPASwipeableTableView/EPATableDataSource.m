//
//  EPATableDataSource.m
//  EPASwipeableTableView
//
//  Created by Евгений Правда on 7/10/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPATableDataSource.h"

NSString * const EPATableCellIdentifier = @"TCell";

@implementation EPATableDataSource


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:EPATableCellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:EPATableCellIdentifier];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"This is row number %d", indexPath.row];
    return cell; 
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 11;
}

@end
