//
//  EPATableView.h
//  EPASwipeableTableView
//
//  Created by Евгений Правда on 7/12/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EPATableView : UITableView

- (void)showTableView;

@end
