//
//  EPATableView.m
//  EPASwipeableTableView
//
//  Created by Евгений Правда on 7/12/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import "EPATableView.h"
#import "EPATableDataSource.h"
@interface EPATableView()
@property (nonatomic, strong) EPATableDataSource *tableDataSource;
@end
@implementation EPATableView

- (id)init {
    self = [super init];
    if (self) {
        [self tableViewInitialize];
    }
    return self;
}

- (void)tableViewInitialize {
    self.tableDataSource = [EPATableDataSource new];
    self.translatesAutoresizingMaskIntoConstraints = NO;
    self.dataSource = self.tableDataSource;
    self.delegate = self.tableDataSource;
    [self setUpGestureForView:self];
}

- (void)hideTableView {

    NSNumber *width = [NSNumber numberWithFloat:[self superview].frame.size.width];
    NSNumber *viewHeight = [NSNumber numberWithFloat:[self superview].frame.size.height];
    [[self superview] removeConstraints:[self superview].constraints];
    NSArray *tableViewWidthConstraints = [NSLayoutConstraint
                                     constraintsWithVisualFormat:@"H:|-(superWidth)-[self(superWidth)]"
                                     options:0
                                     metrics:@{@"superWidth" : width}
                                     views:NSDictionaryOfVariableBindings(self)];
    NSArray *tableViewHeightConstraints = [NSLayoutConstraint
                                          constraintsWithVisualFormat:@"V:|-(0)-[self(viewHeight)]"
                                          options:0
                                           metrics:@{@"viewHeight": viewHeight}
                                          views:NSDictionaryOfVariableBindings(self)];
    [[self superview] addConstraints:tableViewWidthConstraints];
    [[self superview] addConstraints:tableViewHeightConstraints];
    
}

- (void)showTableView {

    NSNumber *viewWidth = [NSNumber numberWithFloat:[self superview].frame.size.width - (([self superview].frame.size.width)/100)*10];
    NSNumber *viewHeight = [NSNumber numberWithFloat:[self superview].frame.size.height];
    NSNumber *leftSpace = [NSNumber numberWithFloat:(([self superview].frame.size.width)/100)*10];
    [[self superview] removeConstraints:[self superview].constraints];
    NSArray *tableViewWidthConstraints = [NSLayoutConstraint
                                          constraintsWithVisualFormat:@"H:|-(leftSpace)-[self(viewWidth)]"
                                          options:0
                                          metrics:@{@"viewWidth" : viewWidth,
                                                    @"leftSpace" : leftSpace}
                                          views:NSDictionaryOfVariableBindings(self)];
    NSArray *tableViewHeightConstraints = [NSLayoutConstraint
                                           constraintsWithVisualFormat:@"V:|-(0)-[self(viewHeight)]"
                                           options:0
                                           metrics:@{@"viewHeight": viewHeight}
                                           views:NSDictionaryOfVariableBindings(self)];
    [[self superview] addConstraints:tableViewWidthConstraints];
    [[self superview] addConstraints:tableViewHeightConstraints];
    
    [UIView animateWithDuration:0.25 animations:^{
        [[self superview] layoutIfNeeded];
    }];
}

- (void)setUpGestureForView:(UIView *)view {
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [view addGestureRecognizer:swipeRight];
}

- (void) handleGesture:(UISwipeGestureRecognizer *)sender {
    [self hideTableView];
    [UIView animateWithDuration:0.25 animations:^{
        [[self superview] layoutIfNeeded];
    }];
}


@end
