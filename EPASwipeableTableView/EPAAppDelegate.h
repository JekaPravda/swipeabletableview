//
//  EPAAppDelegate.h
//  EPASwipeableTableView
//
//  Created by Евгений Правда on 7/10/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EPAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
