//
//  EPATableDataSource.h
//  EPASwipeableTableView
//
//  Created by Евгений Правда on 7/10/15.
//  Copyright (c) 2015 pravda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EPATableDataSource : NSObject <
UITableViewDataSource,
UITableViewDelegate
>

@end
